import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import rootReducer from '../reducers';
// import thunk from 'redux-thunk';

const createStoreWithMiddleware = applyMiddleware(promiseMiddleware())(createStore)
const globalStore = createStoreWithMiddleware(rootReducer);

export default globalStore;