import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import { Provider } from 'react-redux';
import globalStore from './store';
import LoginPage from './components/loginpage';
import UserRoutes from './components/userroutes';
import NotFound from './components/notfound';

class App extends Component {
  render() {
    return (
      <Provider store={globalStore}>
        <Router>
            <Switch>
              <Route exact={true} path="/" component={LoginPage} />
              <Route path="/users" component={UserRoutes} />
              <Route component={NotFound} />
            </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
