import React, { Component } from 'react';
import { OverlayView } from 'react-google-maps'

class RoundMarker extends Component {
    constructor(props){
        super(props);
        this.state = { 
            infoOpen: false
        };
        this.infoToggle = this.infoToggle.bind(this);
    }
    

    infoToggle(){
        this.setState(prevState => ({
            infoOpen : !prevState.infoOpen
        }));
    }

    render(){
        let {lat, lng, image} = this.props;
        return (
            <OverlayView
                position={{ lat, lng}}
                /*
                * An alternative to specifying position is specifying bounds.
                * bounds can either be an instance of google.maps.LatLngBounds
                * or an object in the following format:
                * bounds={{
                *    ne: { lat: 62.400471, lng: -150.005608 },
                *    sw: { lat: 62.281819, lng: -150.287132 }
                * }}
                */
                /*
                * 1. Specify the pane the OverlayView will be rendered to. For
                *    mouse interactivity, use `OverlayView.OVERLAY_MOUSE_TARGET`.
                *    Defaults to `OverlayView.OVERLAY_LAYER`.
                */
                mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                /*
                * 2. Tweak the OverlayView's pixel position. In this case, we're
                *    centering the content.
                */
                getPixelPositionOffset={getPixelPositionOffset}
                /*
                * 3. Create OverlayView content using standard React components.
                */
            >
                <div className="roundmarker" onClick={this.infoToggle} style={{ background: `url(${image})`}}>
                    {this.state.infoOpen && <img src={image} alt=""  className="infoimage"/>}       
                </div>
            </OverlayView>
        )
    }
}


const getPixelPositionOffset = (width, height) => ({
    x: -(width / 2),
    y: -(height / 2),
})

export default RoundMarker;