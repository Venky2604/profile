import React, { Component } from 'react';
import { withGoogleMap, GoogleMap} from 'react-google-maps'
import RoundMarker from './roundmarker';
// import profilepic from "../img/profile.jpg";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { get_geolocation, getusers } from '../actions/geolocation'

 
const GMap = withGoogleMap(props => (
  	<GoogleMap
    	center={props.center}
    	defaultZoom={props.zoom} 
	>   
    {props.users.length ? markers(props.users) : null } 
  	</GoogleMap>
));

const markers = users => users.map( (user, index) => <RoundMarker lat={13.10200 + Math.random(index)} lng={80.2376969 - Math.random(index)} image={user.userprofile.profile_pic} key={user.id} />);


class Map extends Component {
  componentWillMount(){
    this.props.get_geolocation();
    this.props.getusers();
  }
 
  render() {
    let {lat, lng, users} = this.props;
    return(
      <div className="map-container">
        { (lat && lng) ? 
          <GMap
            center={{
              lat, lng
            }}
            zoom={9}
            users={users.userList}
            containerElement={
              <div style={{ height: `100%`}} />
            }
            mapElement={
              <div style={{ height: `100%`}} />
            }
          /> : null
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return ({...state.geolocation_reducer})
};

function mapDispatchToProps(dispatch){
  return bindActionCreators({get_geolocation, getusers},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);