import { combineReducers } from 'redux';
import { geolocation_reducer} from "./geolocation"
import { LoginReducer} from "./loginReducer"

const rootReducer = combineReducers({
    geolocation_reducer,
    LoginReducer
})

export default rootReducer;