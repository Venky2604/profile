let initialstate = {
    lat : 13, 
    lng: 80, 
    locError: false,
    users: {
        isFetching : false,
        userList: [],
        error: false
    }
};

export const geolocation_reducer = (state = initialstate , action) =>{
    switch(action.type){
        case 'GET_LOCATION_PENDING': 
            return {...state}
        case 'GET_LOCATION_FULFILLED': 
            return {...state, lat: action.payload.coords.latitude, lng: action.payload.coords.longitude}
        case 'GET_LOCATION_REJECTED': 
            return {...state, locError: true}
        case 'GET_USERS_PENDING':
            return {...state, users: {...state.users, isFetching:true} }
        case 'GET_USERS_FULFILLED':
            return {...state, users: {...state.users, isFetching:false, userList: action.payload.data} }
        case 'GET_USERS_REJECTED':
            return {...state, users: {...state.users, isFetching:false, error: true} }
        default:
            return state
    }
}