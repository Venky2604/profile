let initialstate = {
    isFetching: false,
    token : localStorage.token ? localStorage.token : '',
    error: false,
    user: localStorage.user ? JSON.parse(localStorage.user) : {} 
};

export const LoginReducer = (state=initialstate, action) => {
    switch(action.type){
        case "LOGIN_PENDING":
            return {...state, isFetching: true} 
        case "LOGIN_FULFILLED":
            return {...state, isFetching: false, token: action.payload.data.token , user: action.payload.data.user } 
        case "LOGIN_REJECTED": 
            return {...state, isFetching: false, isLogin: false, error: action.payload.response.data } 
        default:
            return {...state}
    }
}