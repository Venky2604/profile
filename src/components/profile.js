import React from 'react';

export const ProfileCover = ({profilename, profileimage, active=false}) => (
    <section className="profilecover profile-flex-container">
        <div className="flex-2 order2">
            <div className="profileimagebg">
                <div className="profileimage">
                    <img src={profileimage} className="roundimg" alt=""/> 
                </div>
                {active&& <div className="activeblock">
                    <div className="greenblock"></div>
                </div>}
            </div>
        </div>
        <div className="flex-8 order1 profile-name-flex">
            <div className="profilenamediv">
                <h1 className="profilename">{profilename}</h1>
                <p className="profileage">22 Years Old</p>
            </div>
        </div>   
    </section>
);

export const ProfileLocation = () => (
    <section className="profilelocation">
        <div className="flex-container">
            <div className="flex-2"></div>
        </div>
    </section>
);

export const ProfileDetails = () => (
    <section className="profiledetails">
        <div className="flex-container">
            <div className="flex-2 p10">
                <div className="sidedetaisldiv">
                    <p className="f14">Intersted in</p>
                    <p className="bold">Men</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Lifestyle Expectation</p>
                    <p className="bold">Practical</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Date of Birth</p>
                    <p className="bold">26th April 1996</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Star Sign</p>
                    <p className="bold">Cancer</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Height</p>
                    <p className="bold">170 cm</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Weight</p>
                    <p className="bold">50 kg</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Ethinicity</p>
                    <p className="bold">Indian</p>
                </div>
                <div className="sidedetaisldiv">
                    <p className="f14">Occupation</p>
                    <p className="bold">Model</p>
                </div>
            </div>
            <div className="flex-8 p10">
                <h4>Photos</h4>
                <div className="profilegallery">
                    <img src="https://ia.media-imdb.com/images/M/MV5BMjQ1MjkzOTg5OF5BMl5BanBnXkFtZTgwMjg4NzE0MDI@._V1_QL50_.jpg" alt=""/> 
                    <img src="https://ia.media-imdb.com/images/M/MV5BMjE0NTI2OTMwMF5BMl5BanBnXkFtZTgwMzA4NzE0MDI@._V1_QL50_SY1000_CR0,0,666,1000_AL_.jpg" alt="" /> 
                    <img src="https://ia.media-imdb.com/images/M/MV5BMTA3NTM1OTg5MzNeQTJeQWpwZ15BbWU4MDQ4OTc3ODYx._V1_QL50_.jpg" alt="" />
                    <img src="https://ia.media-imdb.com/images/M/MV5BMTg3NDAwMzQ2MV5BMl5BanBnXkFtZTgwOTk3Nzc4NjE@._V1_QL50_.jpg" alt="" />
                    <img src="https://ia.media-imdb.com/images/M/MV5BMjAzMjYwNjI1OV5BMl5BanBnXkFtZTgwNDY5Nzc4NjE@._V1_QL50_.jpg" alt="" />
                    <img src="https://ia.media-imdb.com/images/M/MV5BODAxNjY0MTc5N15BMl5BanBnXkFtZTgwNDcwODc4NjE@._V1_QL50_.jpg" alt="" />
                    <img src="https://ia.media-imdb.com/images/M/MV5BMTYyNTA3NDU1OV5BMl5BanBnXkFtZTgwMjk3Nzc4NjE@._V1_QL50_.jpg" alt="" />
                </div>
                <h4>About me</h4>
                <div>{`Emily O'Hara Ratajkowski is an American model and actress. Born to American parents in London and raised primarily in California, she rose to prominence in 2013 after appearing in the music video for Robin Thicke's "Blurred Lines", which became the number one song of the year in several countries and attracted controversy over its purportedly sexist content.`}
                </div>
                <h4>What am I looking for</h4>
                <div>{`Ratajkowski co-starred alongside Amy Schumer in the 2018 comedy film I Feel Pretty. Her role, which was described as "pivotal" by USA Today critic Bryan Alexander, included ad libbed improvisational comedy. On March 29, 2018, was cast as a series regular in the NBC pilot Bright Futures.`}</div>
            </div>
        </div>
    </section>
);