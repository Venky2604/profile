import React, { Component } from 'react';
import { ProfileCover, ProfileLocation, ProfileDetails } from './profile';
import emily from "../img/emily.jpg";

class ProfilePage extends Component{
    render(){
        return (
            <div>
                <ProfileCover profilename={"Emily Ratajkowski"} profileimage={emily} active={true}/>
                <ProfileLocation />
                <ProfileDetails />
            </div>
        );
    }
}

export default ProfilePage;