import React, { Component } from 'react';
import "../css/loginpage.css";
import { connect } from 'react-redux';
import { loginAction } from "../actions/login";
import { bindActionCreators } from 'redux';
import loader from "../img/loading.svg";

class LoginPage extends Component{
    
    state = {
        username: '',
        password: '',
        disabled: true
    };

    formSubmit(event){
        event.preventDefault();
        if(!this.state.disabled){
            this.props.loginAction({username: this.state.username, password: this.state.password});
        }
    }

    handleusrChange(event){
        const target = event.target;
        this.setState((prevState) => {
            let valid = target.value && prevState.password.length > 6 ? false : true;
            return{
                [target.name]: target.value,
                disabled: valid
            }
        });
    }

    handlepwdChange(event){
        const target = event.target;
        this.setState((prevState) => {
            let valid = target.value.length > 6 && prevState.username ? false : true;
            return{
                [target.name]: target.value,
                disabled: valid
            }
        });
    }

    render() {
        console.log(this.props);
        return (
            <div className="loginpage">
                <div className="loginform-container">
                    <h1 className="text-center">Login</h1>
                    <form className="loginform" onSubmit={ (e) => this.formSubmit(e) }>
                        <div className="input-container">
                            <span className="fa fa-user inputicon"></span>
                            <input className="logininput" type="text" name="username" onChange={ (e) => this.handleusrChange(e) } placeholder="Username"/>                           
                        </div>
                        <div className="input-container">
                            <span className="fa fa-key inputicon"></span>
                            <input className="logininput" type="password" placeholder="Password" name="password"  onChange={ (e) => this.handlepwdChange(e) } />                           
                        </div>
                        <div className="input-container submitbtn" style={{ opacity: this.state.disabled ? 0.5: 1 }}>
                            { this.props.isFetching ? 
                                <img src={loader} className="loginloader" alt="" /> : 
                                <input className="submitinput" onClick={ (e) => this.formSubmit(e) } type="submit" disabled={this.state.disabled}/>
                            }                        
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return ({...state.LoginReducer})
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({loginAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);