import React, { Component } from 'react';
// import { Link } from "react-router-dom";
import Map from "../map/map";
import { connect } from 'react-redux';
import loader from "../img/loading.svg"

class HomePage extends Component{

    render(){
        return (
            <div className="mappage-container">
              	<div className="flex-container">
					<div className="flex-4">
						{
							this.props.users.isFetching && <div className="loadingdiv"><img src={loader} className="loader" alt="" /></div>
						}
						{
							this.props.users.userList.length ? 
							this.props.users.userList.map(user => <div key={user.id}>{user.first_name}</div> ) : null
						}
					</div>  
					<div className="flex-6">
						<Map />
					</div>  
				</div>
            </div>
        );
    }
}


const mapStateToProps = state => {
	return ({...state.geolocation_reducer})
};

export default connect(mapStateToProps, null)(HomePage) ;