import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import { Header } from './header';
import ProfilePage from './profilepage';
import HomePage from './homepage';
import NotFound from './notfound';

class UserRoutes extends Component {
  render() {
    let { url } = this.props.match; 
    console.log(url);
    return (
        <div className="pagecontainer">
            <Header />
            <Switch>
                <Route exact path={url} component={HomePage} />
                <Route path={`${url}/emily`} component={ProfilePage} />  
                <Route component={NotFound} />
            </Switch>
        </div>  
    );
  }
}

export default UserRoutes;
