import React from 'react';
import profile from "../img/profile.jpg";
import { Link } from "react-router-dom";

export const Header = () => (
    <header className="header">
        <ul className="nav">
            <li className="nav-item"><Link to="/">Home</Link></li>
            <li className="nav-item"><a>About Us</a></li>
            <li className="nav-item"><a>Contact Us</a></li>
        </ul>
        <div className="account-holder dropdown-toggle">
            <div className="account-image">
                <img src={profile} alt="profile" className="roundimg"/>
            </div>
            <div className="dropdown account-dropdown">
                <ul className="nav">
                    <li className="dropdown-item"><a>Profile</a></li>
                    <li className="dropdown-item"><a>Settings</a></li>
                    <li className="dropdown-item"><a>Edit password</a></li>
                    <li className="dropdown-item"><a>Logout</a></li>
                </ul>
            </div>
        </div>
    </header>
);

