import axios from 'axios';

let config = {
    headers: {
        "content-type": "application/json",
    }
}

const url = "http://localhost:4000/api/user/login";

const loginRequest = data => axios.post(url, data, config);

export const loginAction = postdata => ({
    type: "LOGIN",
    payload: loginRequest(postdata)
})