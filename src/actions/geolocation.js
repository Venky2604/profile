import axios from 'axios';

const geolocation = navigator.geolocation; 
const location = new Promise((resolve, reject) => {
    if (!geolocation) {
    reject(new Error('Not Supported'));
    }
    
    geolocation.getCurrentPosition((position) => {
    resolve(position);
    }, () => {
    reject ({error: "something went wrong"});
    });
});

export const get_geolocation = () =>{
    return {
        type: "GET_LOCATION",
        payload: location
    }
}

const getuser_api = axios.get("https://matrimony.billioncart.in/user/list/recently-joined/")

export const getusers = () => ({
    type: "GET_USERS",
    payload:  getuser_api 
})
